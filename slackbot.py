import logging
import time
from slackclient import SlackClient


class SlackBot:
    log = logging.getLogger(__name__)

    def __init__(self, access_key_token):
        self.slack_client = SlackClient(access_key_token)

    # starterbot's ID as an environment variable
    BOT_ID = "U7JPB9DL5"
    # constants
    AT_BOT = "<@" + BOT_ID + ">"
    #EXAMPLE_COMMAND = "do"

    def on_message_received(self,callback):
        self._on_message_received = callback

    def parse_slack_output(self, slack_rtm_output):
        """
            The Slack Real Time Messaging API is an events firehose.
            this parsing function returns None unless a message is
            directed at the Bot, based on its ID.
        """
        output_list = slack_rtm_output
        if output_list and len(output_list) > 0:
            for output in output_list:
                if output and 'text' in output and self.AT_BOT in output['text']:
                    # return text after the @ mention, whitespace removed
                    return output['text'].split(self.AT_BOT)[1].strip().lower(), \
                           output['channel']
        return None, None

    def start_listening(self):
        READ_WEBSOCKET_DELAY = 1  # 1 second delay between reading from firehose
        if self.slack_client.rtm_connect():
            print("StarterBot connected and running!")
            while True:
                command, channel = self.parse_slack_output(self.slack_client.rtm_read())
                if command and channel:
                    # handle_command(command, channel)
                    if self._on_message_received: self._on_message_received(command,channel)
                time.sleep(READ_WEBSOCKET_DELAY)
        else:
            print("Connection failed. Invalid Slack token or bot ID?")


if __name__ == "__main__":
        m = SlackBot()
