import logging as log
import json
import urllib
import codecs


class Game:
    reader = codecs.getreader("utf-8")
    redis = None

    def __init__(self, url):
        self.gameUrl = url
        self._on_game_end = None
        self._on_goal = None
        self._on_scrape = None
        self._on_lead_change = None
        self._on_period_start = None
        self._on_period_end = None
        self._on_game_end = None
        self._on_game_to_start = None
        self.last_event_id_processed = 0
        self.gameStatus = "Scheduled"
        self.current_period = 0
        self.shots_home = 0
        self.shots_away = 0
        self.periods = {}
        self.awayTeam = None
        self.homeTeam = None
        self.game_was_previously_played = False
        self.last_json = None
        self.last_play = None
        self.init()

    def init(self):
        the_game = json.load(Game.reader(urllib.request.urlopen(self.gameUrl)))
        self.awayTeam = Team(the_game['gameData']['teams']['away'])
        self.homeTeam = Team(the_game['gameData']['teams']['home'])

        # self.periods[self.currentPeriod] = Period(the_game['liveData']['linescore']['periods'][self.currentPeriod - 1])
        # let's load period info if there
        for period in the_game['liveData']['linescore']['periods']:
            self.periods[period['num']] = Period(period)

    def get_url(self):
        return self.gameUrl

    def get_team_home(self):
        return self.homeTeam

    def get_team_away(self):
        return self.awayTeam

    def get_game_status(self):
        return self.gameStatus

    def on_goal(self):
        return self._on_goal

    def on_scrape(self, func):
        self._on_scrape = func

    def on_lead_change(self, func):
        self._on_lead_change = func

    def on_goal(self, func):
        self._on_goal = func

    def on_game_to_start(self, func):
        self._on_game_to_start = func

    def on_game_end(self, func):
        self._on_game_end = func

    def on_period_start(self):
        return self._on_period_start

    def on_period_start(self, func):
        self._on_period_start = func

    def on_period_end(self, func):
        self._on_period_end = func

    def current_period(self):
        return self.current_period

    def get_shots_home(self):
        return self.shots_home

    def get_shots_away(self):
        return self.shots_away

    def get_period(self, periodNumber):
        return self.periods[periodNumber]

    def do_scrape(self):
        try:
            the_game = json.load(Game.reader(urllib.request.urlopen(self.gameUrl)))
        except json.JSONDecodeError as er:
            log.exception("Problem reading, current index is: {}".format(self.last_event_id_processed),er)
            return
        except urllib.error.URLError as url_error:
            log.exception(url_error)
            return

        self.last_json = the_game

        if self._on_scrape: self._on_scrape(the_game)

        if Game.redis:
            Game.redis.set(self.gameUrl,the_game)

        self.gameStatus =  the_game['gameData']['status']['detailedState']
        log.debug("Game status: {}".format(self.gameStatus))
        for a_play in the_game['liveData']['plays']['allPlays']:
            try:
                self.current_period = a_play['about']['period']
                # if game is live, we will keep updating the period
                if 'In Progress' in the_game['gameData']['status']['detailedState']:
                    try:
                        if the_game['liveData']['linescore']['currentPeriodTimeRemaining'] != 'Final':
                            self.periods[self.current_period] = Period(
                                the_game['liveData']['linescore']['periods'][int(self.current_period - 1)])
                    except KeyError:
                        log.exception("Couldn't find period for {}".format(self.current_period - 1))
                        pass
                event_index = a_play['about']['eventIdx']
                if event_index > self.last_event_id_processed:
                    self.last_event_id_processed = event_index
                    play_result = a_play['result']
                    # let's update some stats like shots
                    self.shots_home = the_game['liveData']['boxscore']['teams']['home']['teamStats']['teamSkaterStats'][
                        'shots']
                    self.shots_away = the_game['liveData']['boxscore']['teams']['away']['teamStats']['teamSkaterStats'][
                        'shots']

                    log.debug(
                        '[{}]{} event with eventIdx: {}'.format(the_game['gamePk'], play_result['event'], event_index))

                    try:
                        if play_result['eventTypeId'] == 'GOAL':
                            log.debug("Calling goal callback: {}".format(play_result))
                            if self._on_goal: self._on_goal(a_play, self)
                            # if game is tied or +1 or -1 then we must have had a line change
                            home_score = a_play['about']['goals']['home']
                            away_score = a_play['about']['goals']['away']
                            is_scoring_team_home = a_play['team']['triCode'] in self.get_team_home().getAbbreviation()
                            if is_scoring_team_home:
                                if home_score - away_score in [0,1]:
                                    log.debug("We have a lead change: {}".format(a_play))
                                    if self._on_lead_change: self._on_lead_change(a_play, self)
                            else:
                                if away_score - home_score in [0, 1]:
                                    log.debug("We have a lead change: {}".format(a_play))
                                    if self._on_lead_change: self._on_lead_change(a_play, self)
                        elif play_result['eventTypeId'] == 'PERIOD_END':
                            log.debug("Calling period end callback: {}".format(a_play))
                            if self._on_period_end: self._on_period_end(a_play, self)
                        elif play_result['eventTypeId'] == 'PERIOD_START':
                            log.debug("Calling period start callback")
                            if self._on_period_start: self._on_period_start(a_play, self)
                        elif play_result['eventTypeId'] == 'PERIOD_READY' and self.current_period == 1:
                            if self._on_game_to_start: self._on_game_to_start(self)
                        elif play_result['eventTypeId'] == 'GAME_END':
                            if self._on_game_end: self._on_game_end(the_game['liveData']['linescore'], self)
                    except Exception as callback_exception:
                        log.exception(callback_exception)
            except Exception as e:
                log.exception(e)


class Period:
    def __init__(self, periodJson):
        self.json = periodJson

    def get_number(self):
        return self.json['num']

    def get_shots_on_goal_away(self):
        return self.json['away']['shotsOnGoal']

    def get_shots_on_goal_home(self):
        return self.json['home']['shotsOnGoal']


class Team:
    def __init__(self, teamJson):
        self.name = teamJson['name']
        self.abbreviation = teamJson['abbreviation']

    def getName(self):
        return self.name

    def getAbbreviation(self):
        return self.abbreviation


class GoalEvent:
    def __init__(self,json):
        self.json = json
    def get_scorer(self):
        return 'Scorer'

# i think proper name for season is 20192020 ( start end year), need if add non-current season support
class Season:
    def __init__(self, starting_year):
        self.current_season_url = 'https://statsapi.web.nhl.com/api/v1/seasons/current'
        self.schedule_url = 'https://statsapi.web.nhl.com/api/v1/schedule?startDate={}&endDate={}'
        self.starting_year = starting_year
        self.season_summary = json.load(Game.reader(urllib.request.urlopen(self.current_season_url)))
        self.starting_date = '2019-10-02'
        self.ending_date = '2020-04-04'
        self.season_detail = json.load(Game.reader(urllib.request.urlopen(self.schedule_url.format(self.starting_date,self.ending_date))))

        self.total_games = self.season_detail['totalGames']
    # https://statsapi.web.nhl.com/api/v1/schedule?startDate=2019-10-02&endDate=2020-04-04
        self.season_dates = {}
        for game_info in self.season_detail['dates']:
            self.season_dates[game_info['date']] = game_info['games']

    def days_games(self,date):
        return self.season_dates[date]

    def box_scores(self,team):
        games_list = []
        for season_date in self.season_detail['dates']:
            for game in season_date['games']:
                if game['teams']['away']['team']['name'] == team or game['teams']['home']['team']['name'] == team:
                    games_list.append(BoxScore(game))
        return games_list


class BoxScore:
    def __init__(self, game_json):
        self.game_pk = game_json['gamePk']
        self.box_score_url = 'https://statsapi.web.nhl.com/api/v1/game/{}/boxscore'
#        url = self.box_score_url.format(game_json['gamePk'])
        self.game_json = None

    def game_info(self):
        if not self.game_json:
            self.game_json = json.load(Game.reader(urllib.request.urlopen(self.box_score_url.format(self.game_pk))))

        return self.game_json

    def stats(self,player_name):
        away_team = self.game_info()['teams']['away']['players']
        for player in away_team:
            if away_team[player]['person']['fullName'] == player_name:
                return away_team[player]['stats']

        home_team = self.game_info()['teams']['home']['players']
        for player in home_team:
            if home_team[player]['person']['fullName'] == player_name:
                return home_team[player]['stats']

    def goals(self,player_name):
        return self.stats(player_name)['skaterStats']['goals']

    def assists(self, player_name):
        return self.stats(player_name)['skaterStats']['assists']

    def shots(self, player_name):
        return self.stats(player_name)['skaterStats']['shots']

    def hits(self, player_name):
        return self.stats(player_name)['skaterStats']['hits']

    def pims(self, player_name):
        return self.stats(player_name)['skaterStats']['penaltyMinutes']

    def face_off_wins(self, player_name):
        return self.stats(player_name)['skaterStats']['faceOffWins']



