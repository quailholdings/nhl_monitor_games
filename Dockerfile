#FROM python

FROM resin/rpi-raspbian:stretch
MAINTAINER craig

ARG git_commit
ARG version

RUN apt-get update && \
    apt-get -y install  python3 python3-pip python3-setuptools gcc apt-transport-https ca-certificates software-properties-common && \
    apt-get clean

# Install Python requirements
ADD requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt

#ENV TZ=America/Los_Angeles
#RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Create runtime user
RUN useradd pi
RUN mkdir -p /home/pi
RUN usermod -a -G dialout pi
ADD monitor_games.py /home/pi/monitor_games.py
ADD nhlscraping.py /home/pi/nhlscraping.py
ADD slackmessenger.py /home/pi/slackmessenger.py
ADD nhlgamenotificationbot.py /home/pi/nhlgamenotificationbot.py
ADD slackbot.py /home/pi/slackbot.py
RUN chown -R pi /home/pi/
USER root

LABEL git-commit=$git_commit
LABEL version=$version

#RUN chown -R pi /home/pi/
EXPOSE 5000
CMD ["python3","/home/pi/monitor_games.py"]
