import threading
import json
import logging
import os
import urllib
import codecs
from datetime import datetime
from flask import Flask, jsonify, request, Response
from nhlgamenotificationbot import NhlGameNotificationBot, BettingTicket, NhlGameTextFormatter,BettingTextFormatter
from slackmessenger import SlackMessenger
from prometheus_client import Summary, Counter, Gauge, generate_latest

try:
    import graypy
    gray_log_avail = True
except ImportError:
    gray_log_avail = False

# this is for handling the rest calls
reader = codecs.getreader("utf-8")

log = logging.getLogger()


debug_ide = os.getenv('debug_ide') or 0
running_in_ide = os.getenv('running_in_ide') or 0
audit_directory = os.getenv('AUDIT_DIRECTORY') or "/tmp/"
nhl_base_url = os.getenv('GAME_JSON_SERVER_URL') or "https://statsapi.web.nhl.com"
audit_game = int(os.getenv('AUDIT_GAME')) and ("statsapi" in nhl_base_url) or 0

# if graypy avail, let's use it.  unless in debug in ide mode
if gray_log_avail and running_in_ide == 0:
    # let's log to gelf
    gelf_url = os.getenv('GELF_URL') or '192.168.1.20'
    handler = graypy.GELFHandler(gelf_url, 12201, localname='nhl-monitor')
    log.addHandler(handler)
else:
    logging.basicConfig(level=logging.INFO)

log.setLevel(logging.DEBUG)
try:
    log.debug('gelf_url is: {}'.format(gelf_url))
except:
    pass
log.debug("gray_log_avail is: {}".format(gray_log_avail))
log.debug("debug_ide is: {}".format(debug_ide))
log.debug("running_in_ide is: {}".format(running_in_ide))
log.debug("audit_directory is: {}".format(audit_directory))
log.debug("nhl_base_url is: {}".format(nhl_base_url))
log.debug("audit_game is: {}".format(audit_game))


# load swarm secrets
def get_secret(secret_name):
    try:
        with open('/run/secrets/{0}'.format(secret_name), 'r') as secret_file:
            return secret_file.read().rstrip()
    except IOError:
        return None


slack_access_key_id = get_secret('slack_nhl_bot') or os.getenv('slack_nhl_bot')
slack_channel_id = get_secret('slack_nhl_channel_id') or os.getenv('slack_nhl_channel_id')

betting_tickets = {}
slack_messenger = SlackMessenger(slack_access_key_id)
nhl_betting_bot = NhlGameNotificationBot(slack_messenger,BettingTextFormatter(betting_tickets))
nhl_game_watch_bot = NhlGameNotificationBot(slack_messenger,NhlGameTextFormatter())


# if we are debugging, let's hit test server quickly
polling_frequency = float(os.getenv('POLLING_FREQUENCY')or 20)
log.debug("polling_frequency is: {}".format(polling_frequency))
polling_delay_between_games = float(os.getenv('POLLING_DELAY_BETWEEN_GAMES') or 1)
log.debug("polling_delay_between_games is: {}".format(polling_delay_between_games))

nhl_game_watch_bot.polling_frequency_seconds = polling_frequency
nhl_betting_bot.polling_frequency_seconds = polling_frequency
nhl_game_watch_bot.polling_delay_between_games = polling_delay_between_games
nhl_betting_bot.polling_delay_between_games = polling_delay_between_games


# instrumentation
CONTENT_TYPE_LATEST = str('text/plain; version=0.0.4; charset=utf-8')

app = Flask(__name__)

@app.route('/api/games', methods=['GET'])
def get_todays_games():
    log.debug("get_todays_games is called")
    game_json = TodaysGames()
    total_games = game_json['totalGames']

    return_value = "*There are {} game(s) tonight*{}".format(total_games, '\r\n')
    log.debug("total_games is : {}".format(total_games))
    if total_games > 0:
        for game in game_json['dates'][0]['games']:
            return_value += "{} at {}{}".format(game['teams']['away']['team']['name'],
                                            game['teams']['home']['team']['name'], '\r\n')
    return return_value


def get_bet_summary():
    if len(betting_tickets) == 0:
        return "No bets are registered"
    return_value = ""
    for ticket in betting_tickets.values():
        return_value += "{} has bet ${} to win ${}.  Winners are: {}".format(ticket.bettor, ticket.betAmount,
                                                                             ticket.toWinAmount,
                                                                             ticket.winners) + "\r\n"
    return return_value


def get_time_remaining(team):
    lower_case_team = team.lower()
    the_game = next((x for x in NhlGameNotificationBot.gamesToWatch.values() if
                     (lower_case_team in x.get_team_away().getName().lower()) or (
                         lower_case_team in x.get_team_home().getName().lower())),
                    None)
    if the_game:
        game_status = the_game.last_json['gameData']['status']['detailedState']
        log.debug("Game status: {}".format(game_status))
        if 'In Progress' in the_game.last_json['gameData']['status']['detailedState']:
            time_remaining = the_game.last_json['liveData']['linescore']['currentPeriodTimeRemaining']
            the_period = the_game.current_period
            return "{} vs {} has {} remaining in the period {}.".format(the_game.get_team_away().getAbbreviation(),
                                                                        the_game.get_team_home().getAbbreviation(),
                                                                        time_remaining, the_period)
        elif game_status in ['Scheduled', 'Pre-Game']:
            return "{} vs {} has not started".format(the_game.get_team_away().getAbbreviation(),
                                                     the_game.get_team_home().getAbbreviation())
        else:
            return "{} vs {} is a final.".format(the_game.get_team_away().getAbbreviation(),
                                                 the_game.get_team_home().getAbbreviation())
    else:
        return "Cannot find a game with the {}".format(team)


@app.route('/api/lex', methods=['POST'])
def get_game_info():
    try:
        r = json.loads(request.json)
        #  ticket_json = json.loads(request.body.decode("utf-8"))
        intent = r['intentName']
        if intent == 'NHLTimeRemainingInGame':
            return get_time_remaining(r['slots']['Team']), 201
        elif intent == 'TodaysScheduledNhlGames':
            return get_todays_games()
        elif intent == 'ShowBetSummary':
            get_bet_summary()
        elif intent == 'NHLWatchGame':
            return watch_game(team=r['slots']['Team'], url=None)
        else:
            return "NHL does not know how to fulfill: {}".format(intent), 201

        return jsonify({'game': 'worked'}), 201
    except Exception as e:
        log.exception(e)
    return jsonify({'game': 'server error'}), 500


@app.route('/api/channels', methods=['GET'])
def get_slack_channels():
    log.info('retreiving channels')
    return json.dumps(slack_messenger.list_channels(), sort_keys=True, indent=4,
                      separators=(',', ': '))


@app.route('/metrics')
def metrics():
    return Response(generate_latest(), mimetype=CONTENT_TYPE_LATEST)


def submit_game(url, channel_id=slack_channel_id):
    game = nhl_betting_bot.watch(url, channel_id)
    if audit_game == 1: JsonToFileWriter(game)
    return game


def process_tickets_json(ticket_json):
    winners = [x.strip() for x in ticket_json['winners'].split(',')]
    betting_tickets[ticket_json['bettor']] = BettingTicket(bettor=ticket_json['bettor'],
                                                           toWinAmount=ticket_json['payout'],
                                                           betAmount=ticket_json['ticketPrice'], winners=winners)

    if 'urls' in ticket_json:
        for game in ticket_json['urls']:
            a_game = submit_game(game)
            log.debug('Game added: {}'.format(a_game))


@app.route('/api/tickets', methods=['POST'])
def add_ticket():
    log.info("received ticket: {}".format(request))
    process_tickets_json(request.json['ticket'])
    return jsonify({'game': 'worked'}), 201


jeff_url = "http://www.apexdevserver.com/apex/a12276/saturdaynight/picks"
#https://statsapi.web.nhl.com/api/v1/schedule?date=2018-10-14
nhl_todays_games = "https://statsapi.web.nhl.com/api/v1/schedule"


def get_game_url(game_json, participating_team):
    for game in game_json['dates'][0]['games']:
        if game['teams']['away']['team']['name'] == participating_team or game['teams']['home']['team'][
            'name'] == participating_team:
            return nhl_base_url + game['link']


@app.route('/api/grabbets', methods=['POST'])
def grab_bets():
    log.debug("Grabbing bets")
    try:
        the_bets = json.load(reader(urllib.request.urlopen(jeff_url)))
        log.debug("The bets are: {}".format(the_bets))
        game_json = TodaysGames()
        log.debug("Adding bets: {}".format(the_bets))
        for ticket in the_bets['items']:
            name = ticket['username']
            log.debug("Processing ticket for: {}: ".format(name))
            price = ticket['ticketprice']
            to_win = ticket['potential']
            winners = [x.strip() for x in ticket['winners'].split(',')]
            for winner in winners:
                url = get_game_url(game_json, winner)
                submit_game(url)
            bet_ticket = BettingTicket(name, price, to_win, winners)
            log.debug("Betting ticket created: {}".format(bet_ticket))
            betting_tickets[name] = bet_ticket
        return jsonify({'game': 'worked'}), 201
    except Exception as ex:
        log.exception(ex)
        return jsonify({'result': 'Error grabbing bets, check logs'}), 400


def watch_game(team, url):
    log.debug("Looking for game with: {}".format(team))

    if url is None:
        game_json = TodaysGames()
        if game_json['totalGames'] > 0:
            the_game = next((x for x in game_json['dates'][0]['games'] if
                             (team.lower() in x['teams']['home']['team']['name'].lower()) or (
                             team.lower() in x['teams']['away']['team']['name'].lower())),
                            None)
            url = nhl_base_url + the_game['link']
    if url is not None:
        game = nhl_game_watch_bot.watch(url, "C7KPF0AHF")
        if int(audit_game) > 0: JsonToFileWriter(game)
        return "Watching game"
    else:
        return "Team: '{}' not found in todays game list".format(team)


@app.route('/api/games', methods=['POST'])
def add_game_watch():
    log.debug("Request json is: {}".format(request.json))
    team = request.json['team']
    url = None

    if 'url' in request.json:
        url = request.json['url']

    try:
        watch_game(team, url)
        log.debug("We have today's games")
        return jsonify({'result': 'Watching game'}), 201
    except Exception as ex:
        log.exception(ex)
        return jsonify({'result': 'Watching game'}), 400


def flask_thread():
    app.run(host='0.0.0.0')


class TodaysGames(dict):
    def __init__(self):
        today_string = "{:%Y-%m-%d}".format(datetime.now())
        today_url = nhl_todays_games + "?date={}".format(today_string)
        # https://statsapi.web.nhl.com/api/v1/schedule?date=2018-10-14
        self.update(json.load(reader(urllib.request.urlopen(today_url))))


class JsonToFileWriter:
    def __init__(self, game):
        self.game = game
        self.last_event_idx = -1
        game._on_scrape = self.on_scrape

    def on_scrape(self, json_data):
        directory = "{}/{}".format(audit_directory, json_data['gamePk'])

        if not os.path.exists(directory):
            os.makedirs(directory)

        log.debug("Received json for game: {}".format(self.game.get_url()))
        try:
            event_idx = json_data['liveData']['plays']['currentPlay']['about']['eventIdx']
            log.debug("current play idx: {}".format(event_idx))
            if event_idx > self.last_event_idx:
                file_to_write = "{}/{}/{}.json".format(audit_directory, json_data['gamePk'], event_idx)
                log.debug("opening file for write: {}".format(file_to_write))
                with open(file_to_write, 'w') as outfile:
                    json.dump(json_data, outfile)
            self.last_event_idx = event_idx
        except KeyError as ke:
            # this is ok because there is no currentPlay until the game has begun,
            # maybe could watch status to decide to pull this?
            pass


if __name__ == "__main__":
    first_ticket = {
        "bettor": "Craig",
        "ticketPrice": 10,
        "gameDate": "29 - oct - 17",
        "payout": 67.23,
        "winners": "Stars",
        "urls": ["http://localhost:5002/api/v1/game/2017020345/feed/live"]
    }

    if debug_ide == 1:
        process_tickets_json(first_ticket)
        print(get_bet_summary())

    t = threading.Thread(target=flask_thread)
    t.start()
    nhl_betting_bot.watch_games()
