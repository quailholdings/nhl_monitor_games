import logging
import time
from slackclient import SlackClient
#import Queue

log = logging.getLogger(__name__)


class SlackMessenger:

    def __init__(self, access_key_id):
        self.slack_access_key_id = access_key_id
        self.slack_client = SlackClient(access_key_id)
      #  self.sending_queue = Queue()

    def send(self,message, channel_id):
            result = self.slack_client.api_call(
                "chat.postMessage",
                channel=channel_id,
                text=message,
                username='nhlinfo',
                icon_emoji=':robot_face:'
                )

    def list_channels(self):
        channels_call = self.slack_client.api_call("channels.list")
        if channels_call['ok']:
            return channels_call['channels']
        else:
            log.warn(channels_call)
        return None

    def get_users(self):
        api_call = self.slack_client.api_call("users.list")
        if api_call.get('ok'):
            # retrieve all users so we can find our bot
            return api_call.get('members')

    def get_user_info(self,user_name):
        return next((x for x in self.get_users() if x['name'] == user_name), None)

    def wait_to_send(self):
        print("waiting to send")

if __name__ == "__main__":
    m = SlackMessenger()



