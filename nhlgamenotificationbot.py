import time
import logging as log
from nhlscraping import Game
from prometheus_client import Counter
from abc import ABCMeta, abstractmethod

goal_counter = Counter('nhl_scraping_goal_counter', "How many goals scored",['team'])


class AbstractNhlGameTextFormatter:
    @abstractmethod
    def get_goal_text(self, event, game):
        pass

    @abstractmethod
    def get_period_start_text(self, event, game):
        pass

    @abstractmethod
    def get_period_end_text(self, event, game1):
        pass

    @abstractmethod
    def get_game_end_text(self, line_score, game):
        pass

    @abstractmethod
    def get_on_lead_change_text(self, event, game):
        pass


class NhlGameTextFormatter(AbstractNhlGameTextFormatter):

    periodStrings = ['1st', '2nd', '3rd', 'OT']

    def get_goal_text(self, event, game):
        about = event['about']
        result = event['result']
        goal_counter.labels(team=event['team']['name'] ).inc()
        return_string = ("""*{} Goal*: {}
        {}: {}, {}: {} ({} of {})
                """.format(event['team']['name'], result['description'], game.get_team_away().getAbbreviation(),
                           about['goals']['away'],
                           game.get_team_home().getAbbreviation(), about['goals']['home'],
                           about['periodTime'],
                           NhlGameTextFormatter.periodStrings[int(about['period']) - 1]))
        return return_string

    def get_period_start_text(self, event, game):
        return ('{} vs {} - {} period has started.'.format(game.get_team_away().getAbbreviation(),
                                                           game.get_team_home().getAbbreviation(),
                                                           self.periodStrings[
                                                               event['about']['period'] - 1]))

    def get_period_end_text(self, event, game1):
        about = event['about']
        period_info = game1.get_period(about['period'])
        total_home_shots = game1.get_shots_home()
        total_away_shots = game1.get_shots_away()
        period_number = int(about['period'])
        return '{} period has ended, score is {}: {}, {}: {} - Shots on goal this period: {} {}({}) - {} {}({})'.format(
            self.periodStrings[period_number - 1]
            , game1.get_team_away().getAbbreviation(), about['goals']['away'],
            game1.get_team_home().getAbbreviation(),
            about['goals']['home'], game1.get_team_away().getAbbreviation(),
            period_info.get_shots_on_goal_away(),
            total_away_shots,
            game1.get_team_home().getAbbreviation(),
            period_info.get_shots_on_goal_home(), total_home_shots)

    def get_game_end_text(self, line_score, game):
        return '*{}({}) vs {}({}) is final.*'.format(game.get_team_away().getAbbreviation(),
                                                     line_score['teams']['away']['goals'],
                                                     game.get_team_home().getAbbreviation(),
                                                     line_score['teams']['home']['goals'])

    def get_on_lead_change_text(self, event, game):
        return "Lead change detected"


class BettingTextFormatter(NhlGameTextFormatter):
    def __init__(self,betting_tickets):
        self.betting_tickets = betting_tickets
        self.have_sent_betting_preamble = False

    def _get_bettor_list(self, game):
        return_value = []
        for bettor, ticket in self.betting_tickets.items():
            # are any of this tickets winners the home or away team?
            for winner in ticket.winners:
                if winner in game.get_team_home().getName() or winner in game.get_team_away().getName():
                    return_value.append(bettor)
        return return_value

    def get_period_start_text(self, event, game):

        if not self.have_sent_betting_preamble:
            message_lines = []
            self.have_sent_betting_preamble = True
            message_lines.append("*Gentlemen, welcome to saturday night bets.*")
            message_lines.append("Here is the summary of the bets for tonight's games.")

            for bettor, slip in self.betting_tickets.items():
                message_lines.append(
                    "{} has bet ${} to win: ${}.  Winners: {}".format(bettor, slip.bet_amount(), slip.to_win_amount(),
                                                                      slip.winners))
            message_lines.append("Good luck fellas.")

            bettors = self._get_bettor_list(game)
            verb = "has"
            if len(bettors) > 1:
                verb = "have"
            if bettors:
                bettor_action = '*{} {} bet on this game*'.format(','.join(bettors), verb)
            else:
                bettor_action = ''

                message_lines.append('{} vs {} - started.  {}'.format(game.get_team_away().getAbbreviation(),
                                                                      game.get_team_home().getAbbreviation(),
                                                                      bettor_action))
            return "\n".join(message_lines)
        else:
            return None

    def get_goal_text(self, event, game):
        return None

    def get_on_lead_change_text(self, event, game):
        return "*Lead Change* {}({}) at {}({}).  {}".format(game.get_team_away().getAbbreviation(),
                                                     event['about']['goals']['away'],
                                                     game.get_team_home().getAbbreviation(),
                                                     event['about']['goals']['home'],
                                                     event['result']['description']
                                                     )


class NhlGameNotificationBot:
    gamesToWatch = {}
    SCRAPE_CYCLE_COUNT = Counter('nhl_scraping_cycle_count', "How many times we've attempted to do scrape")

    def __init__(self, message_sender, text_formatter):
        self.message_sender = message_sender
        self.channel_for_game_updates = {}
        self.polling_frequency_seconds = 20
        self.polling_delay_between_games = 1
        self.posted_end_game_messages = False
        self.period_end_message_sent = []
        self.text_formatter = text_formatter

    periodStrings = ['1st', '2nd', '3rd', 'OT']

    def watch(self, url, notification_channel):
        if url not in NhlGameNotificationBot.gamesToWatch:
            game = Game(url)
            log.debug("Now watching game: {}".format(game.get_url()))
            game.on_goal(self.on_goal)
            game.on_period_start(self.on_period_start)
            game.on_period_end(self.on_period_end)
            game.on_game_to_start(self.game_to_start)
            game.on_game_end(self.on_game_end)
            game.on_lead_change(self.on_lead_change)
            NhlGameNotificationBot.gamesToWatch[game.get_url()] = game
            self.channel_for_game_updates[game.get_url()] = notification_channel

        else:
            game = NhlGameNotificationBot.gamesToWatch[url]

        return NhlGameNotificationBot.gamesToWatch[game.get_url()]

    def on_goal(self, event, game):
        message_string = self.text_formatter.get_goal_text(event, game)
        if message_string:
            channel_id = self.channel_for_game_updates[game.get_url()]
            self.message_sender.send(message_string, channel_id)

    def on_lead_change(self, event, game):
        message_string = self.text_formatter.get_on_lead_change_text(event, game)
        if message_string:
            self.message_sender.send(message_string, self.channel_for_game_updates[game.get_url()])

    def on_period_start(self, event, game):
        message_string = self.text_formatter.get_period_start_text(event, game)
        if message_string:
            self.message_sender.send(message_string,
                                 self.channel_for_game_updates[game.get_url()])

    def on_period_end(self, event, game1):

        message_string = self.text_formatter.get_period_end_text(event, game1)
        if message_string:
            about = event['about']
            period_number = int(about['period'])
            if period_number not in self.period_end_message_sent:
                self.message_sender.send(message_string, self.channel_for_game_updates[game1.get_url()])
                self.period_end_message_sent.append(period_number)

    def on_game_end(self, line_score, game):
        if self.posted_end_game_messages is False:
            # sometimes they say game is final, but score tied.  let's wait for a final event with non-equal score
            if line_score['teams']['away']['goals'] != line_score['teams']['home']['goals']:
                message_string = self.text_formatter.get_game_end_text(line_score, game)
                if message_string:
                    self.message_sender.send(message_string, self.channel_for_game_updates[game.get_url()])
                    self.posted_end_game_messages = True
                # TODO can we dump 3 stars
                # game['status'] = 'Done'
            else:
                log.info("Ignored game end play because it says game still tied: {}".format(line_score))

    def game_to_start(self, game):
        pass

    @staticmethod
    def get_watched_games():
        return NhlGameNotificationBot.gamesToWatch.values()

    def watch_games(self):
        while True:
            # Code executed here
            NhlGameNotificationBot.SCRAPE_CYCLE_COUNT.inc()
            try:
                for gameKey, game in NhlGameNotificationBot.gamesToWatch.items():
                    if game.get_game_status() != 'Final':
                        game.do_scrape()
                    if self.polling_delay_between_games > 1:
                        time.sleep(self.polling_delay_between_games)
                if self.polling_frequency_seconds > 1:
                    time.sleep(self.polling_frequency_seconds)
            except RuntimeError as re:
                log.exception("error in watch_games")
                # this is ok, someone just added a game


class BettingTicket:
    def __init__(self, bettor, betAmount, toWinAmount, winners):
        self.bettor = bettor
        self.betAmount = betAmount
        self.toWinAmount = toWinAmount
        self.winners = winners

    def isWinner(self):
        return True

    def bet_amount(self):
        return self.betAmount

    def to_win_amount(self):
        return self.toWinAmount

    def winners(self):
        return self.winners


if __name__ == "__main__":
    pass
    # grab_bets()
    # nhl_betting_bot.watch_games()
